//
//  ViewController.m
//  busTicket
//
//  Created by  on 9/24/13.
//  Copyright (c) 2013 seneca. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [[data objectAtIndex:row ]description];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _labelPrice.text = [data objectAtIndex:row];
}


// returns the number of 'columns' to display.
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return [self.model availableSeatsForRoute:_segCity.selectedSegmentIndex]+1;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [self rebuildThePicker];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)segSelectCity:(id)sender {
    [self rebuildThePicker];
}

- (IBAction)btnBuy:(id)sender {
    if([_pickPrice selectedRowInComponent:0] >0){
        NSInteger seats=[_pickPrice selectedRowInComponent:0];
        NSInteger route=[_segCity selectedSegmentIndex];
        [self.model buySeats:seats forRoute:route];
        
        _labelQuanity.text=[NSString stringWithFormat:@"Bought: %@ ",_labelPrice.text];
        
        [self rebuildThePicker];
        [_pickPrice selectRow:0 inComponent:0 animated:YES];
    }
   
}


- (void)rebuildThePicker{
    
    [data removeAllObjects];
    data=[[NSMutableArray alloc] init];
    
    
    NSInteger seats = [self.model availableSeatsForRoute:_segCity.selectedSegmentIndex];
    float price= [self.model ticketCostForRoute:_segCity.selectedSegmentIndex];
    
    for (int i=0; i<=seats; i++) {
        [data addObject:[NSString stringWithFormat:@"%d x %1.2f = %1.2f",i,price,i*price]];
    }
    
    [_pickPrice reloadAllComponents];
    
    
    
}
@end
